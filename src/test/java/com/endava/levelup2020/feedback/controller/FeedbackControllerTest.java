package com.endava.levelup2020.feedback.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import com.endava.levelup2020.feedback.entity.Customer;
import com.endava.levelup2020.feedback.entity.Order;
import com.endava.levelup2020.feedback.entity.Restaurant;
import com.endava.levelup2020.feedback.entity.RestaurantAverageFeedback;
import com.endava.levelup2020.feedback.model.CustomerFeedback;
import com.endava.levelup2020.feedback.model.OrderDetails;
import com.endava.levelup2020.feedback.model.RestaurantFeedback;
import com.endava.levelup2020.feedback.model.UpdateOrder;
import com.endava.levelup2020.feedback.service.CustomerFeedbackService;
import com.endava.levelup2020.feedback.service.OrderFeedbackService;
import com.endava.levelup2020.feedback.service.RestaurantFeedbackService;
import com.endava.levelup2020.feedback.utils.Builders;
import com.endava.levelup2020.feedback.utils.Convertors;
import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ExtendWith(MockitoExtension.class)
class FeedbackControllerTest {

  private static MockMvc mockMvc;

  @InjectMocks
  private static FeedbackController feedbackController;
  @Mock
  private static OrderFeedbackService orderFeedbackService;
  @Mock
  private static CustomerFeedbackService customerFeedbackService;
  @Mock
  private static RestaurantFeedbackService restaurantFeedbackService;
  @Mock
  static Mapper mapper;


  @BeforeAll
  public static void initialize() {
    feedbackController = new FeedbackController(customerFeedbackService,
        restaurantFeedbackService,
        orderFeedbackService);
    mapper = DozerBeanMapperBuilder.create().build();
    mockMvc = MockMvcBuilders.standaloneSetup(feedbackController).build();
  }

  @Nested
  class UpdateOrderDetails {
    @Test
    void updateOrderWithDeliveryManIdTestAccepted() throws Exception {
      UpdateOrder orderParameters = Builders.updateOrderBuilder();
      String orderParametersJson = Convertors.convertObjectToJson(orderParameters);
      when(orderFeedbackService.updateDeliveryManID(orderParameters))
          .thenReturn(true);
      MvcResult result =
          mockMvc.perform(put("/feedback/order/ready")
              .content(orderParametersJson)
              .contentType(MediaType.APPLICATION_JSON))
              .andExpect(status().isOk())
              .andReturn();
      MockHttpServletResponse response = result.getResponse();
      assertEquals(200, response.getStatus());
    }

    @Test
    void updateOrderWithDeliveryManIdTestFailed() throws Exception {
      UpdateOrder orderParameters = Builders.updateOrderBuilder();
      orderParameters.setOrderId(UUID.randomUUID());
      String orderParametersJson = Convertors.convertObjectToJson(orderParameters);
      when(orderFeedbackService.updateDeliveryManID(orderParameters))
          .thenReturn(false);
      MvcResult result =
          mockMvc.perform(put("/feedback/order/ready")
              .content(orderParametersJson)
              .contentType(MediaType.APPLICATION_JSON))
              .andExpect(status().is5xxServerError())
              .andReturn();
      MockHttpServletResponse response = result.getResponse();
      assertEquals(500, response.getStatus());
    }

    @Nested
    class AddOrderDetails {
      OrderDetails orderDetails;
      Order order;

      @BeforeEach
      public void init() {
        orderDetails = new OrderDetails();
        orderDetails.setCustomerId(Builders.uuidFromString(Builders.CUSTOMER_ID));
        orderDetails.setCustomerName("Some guy");
        orderDetails.setOrderId(Builders.uuidFromString(Builders.ORDER_ID));
        orderDetails.setRestaurantId(Builders.uuidFromString(Builders.RESTAURANT_ID));
        orderDetails.setRestaurantName("Some restaurant");

        order = new Order();
        order.setCustomerId(Builders.uuidFromString(Builders.CUSTOMER_ID));
        order.setCustomerName("Some guy");
        order.setOrderId(Builders.uuidFromString(Builders.ORDER_ID));
        order.setRestaurantId(Builders.uuidFromString(Builders.RESTAURANT_ID));
        order.setRestaurantName("Some restaurant");
      }

      @Test
      void whenOrderDetailsInserted() throws Exception {
        when(orderFeedbackService.insert(any())).thenReturn(Optional.of(order));
        String orderDetailsJSON = Convertors.convertObjectToJson(orderDetails);
        MvcResult result = mockMvc.perform(post("/feedback/order")
            .content(orderDetailsJSON)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(200, response.getStatus());
      }

      @Test
      void whenOrderDetailsNotInserted() throws Exception {
        when(mapper.map(orderDetails, Order.class)).thenReturn(order);
        when(orderFeedbackService.insert(order)).thenReturn(Optional.empty());
        String orderDetailsJSON = Convertors.convertObjectToJson(orderDetails);
        // the result
        MvcResult result = mockMvc.perform(post("/feedback/order")
            .content(orderDetailsJSON)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is5xxServerError())
            .andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(500, response.getStatus());
      }
    }

    @Nested
    class addFeedbackForRestaurant {
      RestaurantFeedback restaurantFeedback;
      Restaurant restaurant;

      @BeforeEach
      public void init() {
        restaurantFeedback = new RestaurantFeedback();
        restaurantFeedback.setRestaurantId(Builders.uuidFromString(Builders.RESTAURANT_ID));
        restaurantFeedback.setCustomerId(Builders.uuidFromString(Builders.CUSTOMER_ID));
        restaurantFeedback.setRating(3);
        restaurantFeedback.setReview("Some review");

        restaurant = new Restaurant();
        restaurant.setRestaurantId(Builders.uuidFromString(Builders.RESTAURANT_ID));
        restaurant.setCustomerId(Builders.uuidFromString(Builders.CUSTOMER_ID));
        restaurant.setReview("Some review");
        restaurant.setRating(4);
      }

      @Test
      void whenFeedbackForRestaurantIsInserted() throws Exception {
        when(mapper.map(restaurantFeedback, Restaurant.class)).thenReturn(restaurant);
        when(restaurantFeedbackService.insert(restaurant)).thenReturn(Optional.of(restaurant));
        String restaurantFeedbackJSON = Convertors.convertObjectToJson(restaurantFeedback);
        // the result
        MvcResult result = mockMvc.perform(post("/feedback/restaurant/add")
            .content(restaurantFeedbackJSON)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(200, response.getStatus());

      }

      @Test
      void whenFeedbackForRestaurantIsNotInserted() throws Exception {
        when(mapper.map(restaurantFeedback, Restaurant.class)).thenReturn(restaurant);
        when(restaurantFeedbackService.insert(restaurant)).thenReturn(Optional.empty());
        String restaurantFeedbackJSON = Convertors.convertObjectToJson(restaurantFeedback);
        // the result
        MvcResult result = mockMvc.perform(post("/feedback/restaurant/add")
            .content(restaurantFeedbackJSON)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is5xxServerError())
            .andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(500, response.getStatus());

      }
    }

    @Nested
    class getFeedbacksForRestaurant {
      RestaurantFeedback restaurantFeedback;
      Restaurant restaurant;

      @BeforeEach
      public void init() {
        restaurantFeedback = new RestaurantFeedback();
        restaurantFeedback.setRestaurantId(Builders.uuidFromString(Builders.RESTAURANT_ID));
        restaurantFeedback.setCustomerId(Builders.uuidFromString(Builders.CUSTOMER_ID));
        restaurantFeedback.setRating(3);
        restaurantFeedback.setReview("Some review");

        restaurant = new Restaurant();
        restaurant.setRestaurantId(Builders.uuidFromString(Builders.RESTAURANT_ID));
        restaurant.setCustomerId(Builders.uuidFromString(Builders.CUSTOMER_ID));
        restaurant.setReview("Some review");
        restaurant.setRating(4);
      }

      @Test
      void getAverageFeedbacksForAllRestaurantsTest() throws Exception {
        List<RestaurantAverageFeedback> averageFeedbacks = new ArrayList<>();
        RestaurantAverageFeedback firstAverageFeedback = Builders.averageFeedbackBuilder();
        RestaurantAverageFeedback secondAverageFeedback = Builders.averageFeedbackBuilder();
        averageFeedbacks.add(firstAverageFeedback);
        averageFeedbacks.add(secondAverageFeedback);
        when(restaurantFeedbackService.getAverageRatingsForAllRestaurants())
            .thenReturn(Optional.of(averageFeedbacks));
        MvcResult result = mockMvc.perform(get("/feedback/restaurant/"))
            .andExpect(status().isOk())
            .andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(200, response.getStatus());
      }

      @Test
      void getFeedbacksForRestaurantTest() throws Exception {
        List<Restaurant> feedbacks = new ArrayList<>();
        Restaurant firstAverageFeedback = Builders.restaurantBuilder();
        Restaurant secondAverageFeedback = Builders.restaurantBuilder();
        feedbacks.add(firstAverageFeedback);
        feedbacks.add(secondAverageFeedback);
        when(restaurantFeedbackService.findByRestaurantId(Builders.uuidFromString(Builders.RESTAURANT_ID)))
            .thenReturn(Optional.of(feedbacks));
        MvcResult result =
            mockMvc.perform(get("/feedback/restaurant/"+Builders.RESTAURANT_ID))
                .andExpect(status().isOk())
                .andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(200, response.getStatus());
      }
    }
  }

  @Nested
  class AddFeedbackForCustomer {

    CustomerFeedback customerFeedback;
    Customer customer;

    @BeforeEach
    public void init() {
      customerFeedback = new CustomerFeedback();
      customerFeedback.setDeliveryManId(Builders.uuidFromString(Builders.DELIVERY_MAN_ID));
      customerFeedback.setDetailedReview("some review");
      customerFeedback.setOrderId(Builders.uuidFromString(Builders.ORDER_ID));

      customer = new Customer();
      customer.setDeliveryManId(Builders.uuidFromString(Builders.DELIVERY_MAN_ID));
      customer.setDetailedReview("some review");
      customer.setOrderId(Builders.uuidFromString(Builders.ORDER_ID));
    }

    @Test
    void whenCustomerFeedbackInserted() throws Exception {
      when(mapper.map(customerFeedback, Customer.class)).thenReturn(customer);
      when(customerFeedbackService.insert(customer)).thenReturn(Optional.of(customer));
      String customerFeedbackJSON = Convertors.convertObjectToJson(customerFeedback);
      MvcResult result = mockMvc.perform(post("/feedback/delivery")
          .content(customerFeedbackJSON)
          .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk())
          .andReturn();
      MockHttpServletResponse response = result.getResponse();
      assertEquals(200, response.getStatus());
    }

    @Test
    void whenCustomerFeedbackNotInserted() throws Exception {
      when(mapper.map(customerFeedback, Customer.class)).thenReturn(customer);
      when(customerFeedbackService.insert(customer)).thenReturn(Optional.empty());
      String customerFeedbackJSON = Convertors.convertObjectToJson(customerFeedback);
      // the result
      MvcResult result = mockMvc.perform(post("/feedback/delivery")
          .content(customerFeedbackJSON)
          .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().is5xxServerError())
          .andReturn();
      MockHttpServletResponse response = result.getResponse();
      assertEquals(500, response.getStatus());
    }
  }

}
