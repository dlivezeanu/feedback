package com.endava.levelup2020.feedback;

import static org.junit.jupiter.api.Assertions.assertNotNull;


import com.endava.levelup2020.feedback.controller.FeedbackController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class FeedbackApplicationTests {
  @Autowired
  private FeedbackController feedbackController;
  @Test
  void contextLoads()
  {
    assertNotNull(feedbackController);
  }

}
