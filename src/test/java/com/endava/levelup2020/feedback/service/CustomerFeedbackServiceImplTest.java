package com.endava.levelup2020.feedback.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


import com.endava.levelup2020.feedback.entity.Customer;
import com.endava.levelup2020.feedback.entity.Order;
import com.endava.levelup2020.feedback.repository.CustomerFeedbackRepository;
import com.endava.levelup2020.feedback.repository.OrderFeedbackRepository;
import com.endava.levelup2020.feedback.utils.Builders;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CustomerFeedbackServiceImplTest {

  UUID deliveryManId = Builders.uuidFromString(Builders.DELIVERY_MAN_ID);
  UUID otherDeliveryManId = Builders.uuidFromString(Builders.OTHER_DELIVERY_MAN_ID);
  UUID orderId = Builders.uuidFromString(Builders.ORDER_ID);

  @InjectMocks
  private CustomerFeedbackServiceImpl customerFeedbackService;
  @Mock
  private OrderFeedbackRepository orderFeedbackRepository;
  @Mock
  private CustomerFeedbackRepository customerFeedbackRepository;

  @BeforeEach
  public void init() {
    customerFeedbackService =
        new CustomerFeedbackServiceImpl(customerFeedbackRepository, orderFeedbackRepository);
  }

  @Nested
  class VerifyDeliveryManId {
    @Test
    void whenDeliveryManIdIsSimilarThenAccept() {
      Order order = Builders.buildOrder(deliveryManId);
      when(orderFeedbackRepository.findByOrderId(orderId)).thenReturn(Optional.of(order));
      boolean result = customerFeedbackService.verifyDeliveryManId(deliveryManId, orderId);
      assertTrue(result);
    }

    @Test
    void whenDeliveryManIdIsNotSimilarThenReject() {
      Order order = Builders.buildOrder(otherDeliveryManId);
      when(orderFeedbackRepository.findByOrderId(orderId)).thenReturn(Optional.of(order));
      boolean result = customerFeedbackService.verifyDeliveryManId(deliveryManId, orderId);
      assertFalse(result);
    }

    @Test
    void whenOrderDeliveryManIdNullThenReject() {
      Order order = Builders.buildOrder(null);
      when(orderFeedbackRepository.findByOrderId(orderId)).thenReturn(Optional.of(order));
      boolean result = customerFeedbackService.verifyDeliveryManId(deliveryManId, orderId);
      assertFalse(result);
    }

    @Test
    void whenOrderIsNotFoundThenReject() {
      when(orderFeedbackRepository.findByOrderId(orderId)).thenReturn(Optional.empty());
      boolean result = customerFeedbackService.verifyDeliveryManId(deliveryManId, orderId);
      assertFalse(result);
    }
  }

  @Nested
  class InsertCustomer {
    @Test
    void whenDeliveryManIdIsNull_thenReject() {
      Customer customer = Builders.buildCustomer(null, "Very good",orderId);
      Optional<Customer> customerInserted = customerFeedbackService.insert(customer);
      assertFalse(customerInserted.isPresent());
    }

    @Test
    void whenDetailedReviewIsNull_thenReject() {
      Customer customer = Builders.buildCustomer(deliveryManId, null,orderId);
      Optional<Customer> customerInserted = customerFeedbackService.insert(customer);
      assertFalse(customerInserted.isPresent());
    }

    @Test
    void whenOrderIdIsNull_thenReject() {
      Customer customer = Builders.buildCustomer(deliveryManId,"Very good", null);
      Optional<Customer> customerInserted = customerFeedbackService.insert(customer);
      assertFalse(customerInserted.isPresent());
    }

    @Test
    void whenVerifyDeliveryManIdIsFalse_thenReject() {
      Customer customer = Builders.buildCustomer(deliveryManId,"Very good",orderId);
      Optional<Customer> optionalCustomer = Optional.of(customer);
      when(orderFeedbackRepository.findByOrderId(any())).thenReturn(Optional.empty());
      assertNotEquals(optionalCustomer, customerFeedbackService.insert(customer));
    }

    @Test
    void whenInsertSuccessful_thenAccept() {
      Customer customer = Builders.buildCustomer(deliveryManId,"Very good", orderId);
      Order order = Builders.buildOrder(deliveryManId);
      when(orderFeedbackRepository.findByOrderId(orderId))
          .thenReturn(Optional.of(order));
      when(customerFeedbackRepository.insert((Customer) any())).thenReturn(customer);
      assertEquals(Optional.of(customer), customerFeedbackService.insert(customer));
    }
  }
}