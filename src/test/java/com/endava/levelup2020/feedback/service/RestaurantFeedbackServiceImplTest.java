package com.endava.levelup2020.feedback.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


import com.endava.levelup2020.feedback.entity.Restaurant;
import com.endava.levelup2020.feedback.entity.RestaurantAverageFeedback;
import com.endava.levelup2020.feedback.repository.RestaurantFeedbackRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;


@ExtendWith(MockitoExtension.class)
class RestaurantFeedbackServiceImplTest {
  public static final String RESTAURANT_ID = "5b02f34c-0ad9-4d3d-92b6-9cdf7b85c46a";
  public static final String CUSTOMER_ID = "5b02f34c-0ad9-4d3d-92b6-9cdf7b85c46a";
  public static final String REVIEW = "Foarte bun";
  public static final int NUMBER_OF_RATINGS_INTEGER = 5;
  public static final double AVERAGE_RATING_INTEGER = 4.1;
  public static final int RATING_VALUE = 4;

  @InjectMocks
  private static RestaurantFeedbackService restaurantFeedbackService;
  @Mock
  private static RestaurantFeedbackRepository restaurantFeedbackRepository;
  @Mock
  private static MongoTemplate mongoTemplate;

  @BeforeAll
  static void init() {
    restaurantFeedbackService =
        new RestaurantFeedbackServiceImpl(restaurantFeedbackRepository, mongoTemplate);
  }

  @Test
  void insertsAndReturnsTheRestaurantIfInserted() {
    Restaurant restaurant = buildRestaurant(uuidFromString(RESTAURANT_ID));
    when(restaurantFeedbackRepository.insert(restaurant))
        .thenReturn(restaurant);
    boolean result = restaurantFeedbackService.insert(restaurant)
        .equals(Optional.of(restaurant));
    assertTrue(result);
  }

  @Test
  void returnsAllTheFeedbacksBasedOnTheRestaurantId() {
    Restaurant firstFeedback = buildRestaurant(uuidFromString(RESTAURANT_ID));
    Restaurant secondFeedback = buildRestaurant(uuidFromString(RESTAURANT_ID));
    List<Restaurant> feedbackList = new ArrayList<>();
    feedbackList.add(firstFeedback);
    feedbackList.add(secondFeedback);
    when(restaurantFeedbackRepository.findByRestaurantId(uuidFromString(RESTAURANT_ID)))
        .thenReturn(Optional.of(feedbackList));
    boolean result =
        restaurantFeedbackService.findByRestaurantId(uuidFromString(RESTAURANT_ID)).get()
            .equals(feedbackList);
    assertTrue(result);
  }

  @Test
  void returnsAllTheFeedbacksFromAllRestaurants() {
    RestaurantAverageFeedback averageRatingForFirstRestaurant = buildAverageRating();
    RestaurantAverageFeedback averageRatingForSecondRestaurant = buildAverageRating();
    List<RestaurantAverageFeedback> averageRatingForAllRestaurantsList = new ArrayList<>();
    averageRatingForAllRestaurantsList.add(averageRatingForFirstRestaurant);
    averageRatingForAllRestaurantsList.add(averageRatingForSecondRestaurant);
    AggregationResults<RestaurantAverageFeedback> resultMock = mock(AggregationResults.class);
    when(resultMock.getMappedResults()).thenReturn(averageRatingForAllRestaurantsList);
    doReturn(resultMock)
        .when(mongoTemplate)
        .aggregate(any(), eq(Restaurant.class), eq(RestaurantAverageFeedback.class));
    Optional<List<RestaurantAverageFeedback>> result =
        restaurantFeedbackService.getAverageRatingsForAllRestaurants();
    assertNotNull(result);
  }


  private RestaurantAverageFeedback buildAverageRating() {
    RestaurantAverageFeedback restaurantAverageFeedback = new RestaurantAverageFeedback();
    restaurantAverageFeedback.setAverageRating(AVERAGE_RATING_INTEGER);
    restaurantAverageFeedback.setNumberOfRatings(NUMBER_OF_RATINGS_INTEGER);
    restaurantAverageFeedback.setRestaurantId(uuidFromString(RESTAURANT_ID));
    return restaurantAverageFeedback;
  }

  private Restaurant buildRestaurant(UUID restaurantId) {
    Restaurant restaurant = new Restaurant();
    restaurant.setRestaurantId(restaurantId);
    restaurant.setCustomerId(uuidFromString(CUSTOMER_ID));
    restaurant.setRating(RATING_VALUE);
    restaurant.setReview(REVIEW);
    return restaurant;
  }

  private UUID uuidFromString(String uuidString) {
    return UUID.fromString(uuidString);
  }

}