package com.endava.levelup2020.feedback.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


import com.endava.levelup2020.feedback.entity.Order;
import com.endava.levelup2020.feedback.model.UpdateOrder;
import com.endava.levelup2020.feedback.repository.OrderFeedbackRepository;
import com.endava.levelup2020.feedback.utils.Builders;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OrderFeedbackServiceImplTest {

  @InjectMocks
  private OrderFeedbackServiceImpl orderFeedbackService;
  @Mock
  private OrderFeedbackRepository orderFeedbackRepository;

  private Order order;

  @BeforeEach
  public void init()
  {
    orderFeedbackService = new OrderFeedbackServiceImpl(orderFeedbackRepository);
    order = Builders.buildOrder(Builders.uuidFromString(Builders.DELIVERY_MAN_ID));
  }

  @Test
  void whenGetOrders(){
    assertNotNull(orderFeedbackService.getOrders());
  }
  @Nested
  class InsertOrder{

    @Test
    void whenCustomerIdIsNull_thenReject(){
      order.setCustomerId(null);
      assertFalse(orderFeedbackService.insert(order).isPresent());
    }
    @Test
    void whenCustomerNameIsNull_thenReject(){
      order.setCustomerName(null);
      assertFalse(orderFeedbackService.insert(order).isPresent());
    }
    @Test
    void whenOrderIdIsNull_thenReject(){
      order.setOrderId(null);
      assertFalse(orderFeedbackService.insert(order).isPresent());
    }
    @Test
    void whenRestaurantIdIsNull_thenReject(){
      order.setRestaurantId(null);
      assertFalse(orderFeedbackService.insert(order).isPresent());
    }
    @Test
    void whenRestaurantNameIsNull_thenReject(){
      order.setRestaurantName(null);
      assertFalse(orderFeedbackService.insert(order).isPresent());
    }
    @Test
    void whenAllConditionsSatisfied_thenAccept(){
      when(orderFeedbackRepository.insert((Order) any())).thenReturn(order);
      assertEquals(Optional.of(order),orderFeedbackService.insert(order));
    }
  }

  @Nested
  class updateDeliveryManId{

    private UpdateOrder updateOrder;

    @BeforeEach
    void init(){
      updateOrder = Builders.buildUpdateOrder();
    }

    @Test
    void whenOrderDoesNotExist_thenReject(){
      when(orderFeedbackRepository.findById(updateOrder.getOrderId()))
          .thenReturn(Optional.empty());
      assertFalse(orderFeedbackService.updateDeliveryManID(updateOrder));
    }
    @Test
    void whenOrderExists_thenAccept(){
      when(orderFeedbackRepository.findById(updateOrder.getOrderId()))
          .thenReturn(Optional.of(order));
      assertTrue(orderFeedbackService.updateDeliveryManID(updateOrder));

    }
  }

}
