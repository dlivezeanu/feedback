package com.endava.levelup2020.feedback.utils;

import com.endava.levelup2020.feedback.entity.Customer;
import com.endava.levelup2020.feedback.entity.Order;
import com.endava.levelup2020.feedback.entity.Restaurant;
import com.endava.levelup2020.feedback.entity.RestaurantAverageFeedback;
import com.endava.levelup2020.feedback.model.RestaurantFeedback;
import com.endava.levelup2020.feedback.model.UpdateOrder;
import java.util.UUID;

public class Builders {
  public static final String DELIVERY_MAN_ID = "5b02f34c-0ad9-4d3d-92b6-9cdf7b85c46a";
  public static final String ORDER_ID = "c05ee03f-a6e1-4951-8f14-d4c42292587e";
  public static final String RESTAURANT_ID = "44803d6e-08aa-4323-b96d-88025835b081";
  public static final String CUSTOMER_ID = "475c5451-d959-4877-ac68-d11e2eb71c03";
  public static final String OTHER_DELIVERY_MAN_ID = "77732ff7-2f23-4507-9bb1-312097233112";

  public static Customer buildCustomer(UUID deliveryManId, String detailedReview, UUID orderId) {
    Customer customer = new Customer();
    customer.setDeliveryManId(deliveryManId);
    customer.setDetailedReview(detailedReview);
    customer.setOrderId(orderId);
    return customer;
  }

  public static Order buildOrder(UUID orderDeliveryManId) {
    Order order = new Order();
    order.setDeliveryManId(orderDeliveryManId);
    order.setCustomerId(uuidFromString(CUSTOMER_ID));
    order.setCustomerName("John");
    order.setRestaurantId(uuidFromString(RESTAURANT_ID));
    order.setRestaurantName("Papa John");
    order.setOrderId(uuidFromString(ORDER_ID));
    return order;
  }

  public static UpdateOrder buildUpdateOrder() {
    UpdateOrder updateOrder = new UpdateOrder();
    updateOrder.setDeliveryManId(uuidFromString(DELIVERY_MAN_ID));
    updateOrder.setOrderId(uuidFromString(ORDER_ID));
    return updateOrder;
  }

  public static UpdateOrder updateOrderBuilder() {
    UpdateOrder orderParameters = new UpdateOrder();
    orderParameters.setDeliveryManId(uuidFromString(DELIVERY_MAN_ID));
    orderParameters.setOrderId(uuidFromString(ORDER_ID));
    return orderParameters;
  }

  public static RestaurantAverageFeedback averageFeedbackBuilder() {
    RestaurantAverageFeedback averageFeedback = new RestaurantAverageFeedback();
    averageFeedback.setAverageRating(5.4);
    averageFeedback.setNumberOfRatings(12);
    averageFeedback.setRestaurantId(uuidFromString(RESTAURANT_ID));
    return averageFeedback;
  }

  public static Restaurant restaurantBuilder() {
    Restaurant restaurant = new Restaurant();
    restaurant.setReview("Foarte Bun");
    restaurant.setRating(5);
    restaurant.setCustomerId(uuidFromString(CUSTOMER_ID));
    restaurant.setRestaurantId(uuidFromString(RESTAURANT_ID));
    return restaurant;
  }


  public static UUID uuidFromString(String uuidString) {
    return UUID.fromString(uuidString);
  }
}
