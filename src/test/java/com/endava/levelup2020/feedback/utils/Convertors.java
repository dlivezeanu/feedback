package com.endava.levelup2020.feedback.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.io.IOException;


public class Convertors {

  public static String convertObjectToJson(Object object){
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JavaTimeModule());
    String stringValue = "";
    try {
      stringValue = objectMapper.writeValueAsString(object);
    } catch (JsonProcessingException e) {
      System.out.println("Exception "+ e);
    }
    return stringValue;
  }
  public static <T> T convertJsonToObject(String json, TypeReference<T> type) throws IOException {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JavaTimeModule());
    return objectMapper.readValue(json, type);
  }




}
