package com.endava.levelup2020.feedback.controller;


import com.endava.levelup2020.feedback.entity.Customer;
import com.endava.levelup2020.feedback.entity.Order;
import com.endava.levelup2020.feedback.entity.Restaurant;
import com.endava.levelup2020.feedback.entity.RestaurantAverageFeedback;
import com.endava.levelup2020.feedback.model.AverageFeedback;
import com.endava.levelup2020.feedback.model.CustomerFeedback;
import com.endava.levelup2020.feedback.model.OrderDetails;
import com.endava.levelup2020.feedback.model.RestaurantFeedback;
import com.endava.levelup2020.feedback.model.UpdateOrder;
import com.endava.levelup2020.feedback.service.CustomerFeedbackService;
import com.endava.levelup2020.feedback.service.OrderFeedbackService;
import com.endava.levelup2020.feedback.service.RestaurantFeedbackService;
import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class FeedbackController implements FeedbackApi {
  Mapper mapper = DozerBeanMapperBuilder.create().build();

  private CustomerFeedbackService customerFeedbackService;
  private RestaurantFeedbackService restaurantFeedbackService;
  private OrderFeedbackService orderFeedbackService;


  @Autowired
  public FeedbackController(CustomerFeedbackService customerFeedbackService,
                            RestaurantFeedbackService restaurantFeedbackService,
                            OrderFeedbackService orderFeedbackService
  ) {
    this.customerFeedbackService = customerFeedbackService;
    this.restaurantFeedbackService = restaurantFeedbackService;
    this.orderFeedbackService = orderFeedbackService;
  }

  @Override
  public ResponseEntity<Void> addOrderDetails(@Valid OrderDetails orderDetails) {
    Order order = mapper.map(orderDetails, Order.class);
    Optional<Order> insertedOrder = orderFeedbackService.insert(order);
    if (insertedOrder.isPresent()) {
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Override
  public ResponseEntity<Void> addFeedbackForRestaurant(
      @Valid RestaurantFeedback restaurantFeedback) {
    Restaurant restaurant = mapper.map(restaurantFeedback, Restaurant.class);
    Optional<Restaurant> insertedRestaurant = restaurantFeedbackService.insert(restaurant);
    if (insertedRestaurant.isPresent()) {
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Override
  public ResponseEntity<List<RestaurantFeedback>> getFeedbacksForRestaurant(UUID restaurantId) {
    Optional<List<Restaurant>> restaurants =
        restaurantFeedbackService.findByRestaurantId(restaurantId);
    return ResponseEntity.ok(
        restaurants.map(restaurantFeedback -> restaurantFeedback.stream()
            .map(feedback -> mapper.map(feedback, RestaurantFeedback.class))
            .collect(Collectors.toList())).orElseGet(ArrayList::new));
  }

  @Override
  public ResponseEntity<List<AverageFeedback>> getAverageFeedbacksForAllRestaurants() {
    Optional<List<RestaurantAverageFeedback>> averageFeedbacks =
        restaurantFeedbackService.getAverageRatingsForAllRestaurants();
    return ResponseEntity.ok(
        averageFeedbacks.map(restaurantAverageFeedbacks -> restaurantAverageFeedbacks.stream()
            .map(feedback -> mapper.map(feedback, AverageFeedback.class))
            .collect(Collectors.toList())).orElseGet(ArrayList::new));
  }

  @Override
  public ResponseEntity<Void> addFeedbackForCustomer(@Valid CustomerFeedback customerFeedback) {
    Customer customer = mapper.map(customerFeedback, Customer.class);
    Optional<Customer> insertedCustomer = customerFeedbackService.insert(customer);
    if (insertedCustomer.isPresent()) {
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @Override
  public ResponseEntity<Void> updateOrderWithDeliveryManId(@Valid UpdateOrder orderParameters) {
    boolean updated = orderFeedbackService.updateDeliveryManID(orderParameters);
    if (updated) {
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }
}

