package com.endava.levelup2020.feedback.entity;

import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Order")
public class Order {
  @Id
  private UUID orderId;
  private UUID restaurantId;
  private String restaurantName;
  private UUID customerId;
  private String customerName;
  private UUID deliveryManId;

  public Order() {
    deliveryManId = null;
  }

  public Order(UUID orderId, UUID restaurantId, String restaurantName, UUID customerId,
               String customerName) {
    this.orderId = orderId;
    this.restaurantId = restaurantId;
    this.restaurantName = restaurantName;
    this.customerId = customerId;
    this.customerName = customerName;
  }

  public UUID getDeliveryManId() {
    return deliveryManId;
  }

  public void setDeliveryManId(UUID deliveryManId) {
    this.deliveryManId = deliveryManId;
  }

  public UUID getOrderId() {
    return orderId;
  }

  public void setOrderId(UUID orderId) {
    this.orderId = orderId;
  }

  public UUID getRestaurantId() {
    return restaurantId;
  }

  public void setRestaurantId(UUID restaurantId) {
    this.restaurantId = restaurantId;
  }

  public String getRestaurantName() {
    return restaurantName;
  }

  public void setRestaurantName(String restaurantName) {
    this.restaurantName = restaurantName;
  }

  public UUID getCustomerId() {
    return customerId;
  }

  public void setCustomerId(UUID customerId) {
    this.customerId = customerId;
  }

  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }


}
