package com.endava.levelup2020.feedback.entity;

import java.util.UUID;

public class RestaurantAverageFeedback {
  private UUID restaurantId;
  private Integer numberOfRatings;
  private Double averageRating;

  public RestaurantAverageFeedback() {

  }

  public RestaurantAverageFeedback(UUID restaurantId, Integer numberOfRatings,
                                   Double averageRating) {
    this.restaurantId = restaurantId;
    this.numberOfRatings = numberOfRatings;
    this.averageRating = averageRating;
  }

  public UUID getRestaurantId() {
    return restaurantId;
  }

  public void setRestaurantId(UUID restaurantId) {
    this.restaurantId = restaurantId;
  }

  public Integer getNumberOfRatings() {
    return numberOfRatings;
  }

  public void setNumberOfRatings(Integer numberOfRatings) {
    this.numberOfRatings = numberOfRatings;
  }

  public Double getAverageRating() {
    return averageRating;
  }

  public void setAverageRating(Double averageRating) {
    this.averageRating = averageRating;
  }


}
