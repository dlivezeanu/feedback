package com.endava.levelup2020.feedback.entity;

import java.util.UUID;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Customer")
public class Customer {
  private UUID orderId;
  private UUID deliveryManId;
  private String detailedReview;

  public Customer() {

  }

  public Customer(UUID orderId, UUID deliveryManId, String detailedReview) {
    this.orderId = orderId;
    this.deliveryManId = deliveryManId;
    this.detailedReview = detailedReview;
  }

  public UUID getOrderId() {
    return orderId;
  }

  public void setOrderId(UUID orderId) {
    this.orderId = orderId;
  }

  public UUID getDeliveryManId() {
    return deliveryManId;
  }

  public void setDeliveryManId(UUID deliveryManId) {
    this.deliveryManId = deliveryManId;
  }

  public String getDetailedReview() {
    return detailedReview;
  }

  public void setDetailedReview(String detailedReview) {
    this.detailedReview = detailedReview;
  }


}
