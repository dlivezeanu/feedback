package com.endava.levelup2020.feedback.entity;

import java.util.UUID;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Restaurant")
public class Restaurant {
  private UUID restaurantId;
  private UUID customerId;
  private Integer rating;
  private String review;

  public Restaurant() {

  }

  public Restaurant(UUID restaurantId, UUID customerId, Integer rating, String review) {
    this.restaurantId = restaurantId;
    this.customerId = customerId;
    this.rating = rating;
    this.review = review;
  }

  public UUID getRestaurantId() {
    return restaurantId;
  }

  public void setRestaurantId(UUID restaurantId) {
    this.restaurantId = restaurantId;
  }

  public UUID getCustomerId() {
    return customerId;
  }

  public void setCustomerId(UUID customerId) {
    this.customerId = customerId;
  }

  public Integer getRating() {
    return rating;
  }

  public void setRating(Integer rating) {
    this.rating = rating;
  }

  public String getReview() {
    return review;
  }

  public void setReview(String review) {
    this.review = review;
  }

}
