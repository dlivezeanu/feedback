package com.endava.levelup2020.feedback.utils;

public final class TableFieldsConstant {
  private TableFieldsConstant() {
  }

  public static final String RESTAURANT_ID = "restaurantId";
}
