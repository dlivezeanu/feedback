package com.endava.levelup2020.feedback.repository;

import com.endava.levelup2020.feedback.entity.Order;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderFeedbackRepository extends MongoRepository<Order, UUID> {
  Optional<Order> findByOrderId(UUID orderID);

}
