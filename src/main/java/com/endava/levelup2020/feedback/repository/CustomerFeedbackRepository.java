package com.endava.levelup2020.feedback.repository;

import com.endava.levelup2020.feedback.entity.Customer;
import java.util.UUID;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CustomerFeedbackRepository extends MongoRepository<Customer, UUID> {

}
