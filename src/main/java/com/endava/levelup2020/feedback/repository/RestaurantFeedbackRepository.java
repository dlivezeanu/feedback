package com.endava.levelup2020.feedback.repository;


import com.endava.levelup2020.feedback.entity.Restaurant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RestaurantFeedbackRepository extends MongoRepository<Restaurant, UUID> {
  Optional<List<Restaurant>> findByRestaurantId(UUID restaurantId);
}
