package com.endava.levelup2020.feedback.service;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;


import com.endava.levelup2020.feedback.entity.Restaurant;
import com.endava.levelup2020.feedback.entity.RestaurantAverageFeedback;
import com.endava.levelup2020.feedback.repository.RestaurantFeedbackRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.stereotype.Service;


@Service
public class RestaurantFeedbackServiceImpl implements RestaurantFeedbackService {

  public static final String RESTAURANT_ID = "restaurantId";
  public static final String NUMBER_OF_RATINGS = "numberOfRatings";
  public static final String AVERAGE_RATING = "averageRating";
  public static final String RATING = "rating";

  private RestaurantFeedbackRepository restaurantFeedbackRepository;
  private MongoTemplate mongoTemplate;

  @Autowired
  public RestaurantFeedbackServiceImpl(RestaurantFeedbackRepository restaurantFeedbackRepository,
                                       MongoTemplate mongoTemplate) {
    this.restaurantFeedbackRepository = restaurantFeedbackRepository;
    this.mongoTemplate = mongoTemplate;
  }

  @Override
  public Optional<Restaurant> insert(Restaurant restaurant) {
    return Optional.of(restaurantFeedbackRepository.insert(restaurant));
  }

  @Override
  public Optional<List<Restaurant>> findByRestaurantId(UUID id) {
    return restaurantFeedbackRepository.findByRestaurantId(id);
  }

  @Override
  public Optional<List<RestaurantAverageFeedback>> getAverageRatingsForAllRestaurants() {
    ProjectionOperation projectionOperation =
        project(RESTAURANT_ID, NUMBER_OF_RATINGS, AVERAGE_RATING);
    Aggregation agregation = Aggregation.newAggregation(
        getGroupOperation(),
        projectionOperation
    );
    return Optional
        .of(mongoTemplate.aggregate(agregation, Restaurant.class, RestaurantAverageFeedback.class)
            .getMappedResults());
  }

  private GroupOperation getGroupOperation() {
    return group(RESTAURANT_ID)
        .addToSet(RESTAURANT_ID).as(RESTAURANT_ID)
        .avg(RATING).as(AVERAGE_RATING)
        .count().as(NUMBER_OF_RATINGS);
  }
}
