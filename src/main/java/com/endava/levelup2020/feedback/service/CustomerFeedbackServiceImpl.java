package com.endava.levelup2020.feedback.service;

import com.endava.levelup2020.feedback.entity.Customer;
import com.endava.levelup2020.feedback.entity.Order;
import com.endava.levelup2020.feedback.repository.CustomerFeedbackRepository;
import com.endava.levelup2020.feedback.repository.OrderFeedbackRepository;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerFeedbackServiceImpl implements CustomerFeedbackService {

  private CustomerFeedbackRepository customerFeedbackRepository;
  private OrderFeedbackRepository orderFeedbackRepository;

  @Autowired
  public CustomerFeedbackServiceImpl(CustomerFeedbackRepository customerFeedbackRepository,
                                     OrderFeedbackRepository orderFeedbackRepository) {
    this.customerFeedbackRepository = customerFeedbackRepository;
    this.orderFeedbackRepository = orderFeedbackRepository;
  }

  @Override
  public Optional<Customer> insert(Customer customerFeedback) {
    if (customerFeedback.getDeliveryManId() != null &&
        customerFeedback.getDetailedReview() != null &&
        customerFeedback.getOrderId() != null &&
        verifyDeliveryManId(customerFeedback.getDeliveryManId(), customerFeedback.getOrderId())) {
      return Optional.of(customerFeedbackRepository.insert(customerFeedback));
    }
    return Optional.empty();
  }

  @Override
  public boolean verifyDeliveryManId(UUID deliveryManId, UUID orderId) {
    Optional<Order> orderToBeVerified = orderFeedbackRepository.findByOrderId(orderId);
    if (orderToBeVerified.isPresent() &&
        orderToBeVerified.get().getDeliveryManId() != null) {
      return orderToBeVerified.get().getDeliveryManId().equals(deliveryManId);
    }
    return false;
  }
}
