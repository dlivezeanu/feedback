package com.endava.levelup2020.feedback.service;

import com.endava.levelup2020.feedback.entity.Customer;
import java.util.Optional;
import java.util.UUID;

public interface CustomerFeedbackService {
  Optional<Customer> insert(Customer customerFeedback);

  boolean verifyDeliveryManId(UUID deliveryManId, UUID orderId);
}
