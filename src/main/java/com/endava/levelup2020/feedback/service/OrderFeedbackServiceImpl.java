package com.endava.levelup2020.feedback.service;

import com.endava.levelup2020.feedback.entity.Order;
import com.endava.levelup2020.feedback.model.UpdateOrder;
import com.endava.levelup2020.feedback.repository.OrderFeedbackRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderFeedbackServiceImpl implements OrderFeedbackService {

  private final OrderFeedbackRepository orderFeedbackRepository;

  @Autowired
  public OrderFeedbackServiceImpl(
      OrderFeedbackRepository orderFeedbackRepository) {
    this.orderFeedbackRepository = orderFeedbackRepository;
  }

  @Override
  public List<Order> getOrders() {
    return orderFeedbackRepository.findAll();
  }

  @Override
  public Optional<Order> insert(Order order) {
    if (order.getCustomerId() != null &&
        order.getCustomerName() != null &&
        order.getOrderId() != null &&
        order.getRestaurantId() != null &&
        order.getRestaurantName() != null) {
      return Optional.of(orderFeedbackRepository.insert(order));
    }
    return Optional.empty();
  }

  @Override
  public boolean updateDeliveryManID(UpdateOrder orderParameters) {
    Optional<Order> orderToBeUpdated =
        orderFeedbackRepository.findById(orderParameters.getOrderId());
    if (orderToBeUpdated.isPresent()) {
      orderToBeUpdated.get().setDeliveryManId(orderParameters.getDeliveryManId());
      orderFeedbackRepository.save(orderToBeUpdated.get());
      return true;
    }
    return false;
  }
}
