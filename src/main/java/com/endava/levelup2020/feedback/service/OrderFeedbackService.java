package com.endava.levelup2020.feedback.service;

import com.endava.levelup2020.feedback.entity.Order;
import com.endava.levelup2020.feedback.model.UpdateOrder;
import java.util.List;
import java.util.Optional;

public interface OrderFeedbackService {
  List<Order> getOrders();

  Optional<Order> insert(Order order);

  boolean updateDeliveryManID(UpdateOrder orderParameters);

}
