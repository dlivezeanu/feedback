package com.endava.levelup2020.feedback.service;

import com.endava.levelup2020.feedback.entity.Restaurant;
import com.endava.levelup2020.feedback.entity.RestaurantAverageFeedback;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface RestaurantFeedbackService {
  Optional<Restaurant> insert(Restaurant restaurant);

  Optional<List<Restaurant>> findByRestaurantId(UUID restaurantId);

  Optional<List<RestaurantAverageFeedback>> getAverageRatingsForAllRestaurants();
}
